<?php

namespace Satanik\IoC\Concerns;

trait Resolveable
{
    /**
     * @return mixed|self
     */
    public static function resolve()
    {
        return resolve(self::class);
    }

    /**
     * @return mixed|self
     */
    public static function bound()
    {
        return app()->make(self::class, ['class' => true]);
    }
}
