<?php

namespace Satanik\IoC\Contracts;

interface Resolveable
{
    public static function resolve();

    public static function bound();
}
