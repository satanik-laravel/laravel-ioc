# Satanik IoC Package

[![Software License][ico-license]](LICENSE.md)

This package simply adds the functionality to faster access bound class by the service container.

## Structure

The directory structure follows the industry standard.

```bash
config/
src/
tests/
```


## Install

Via Composer

``` bash
$ composer require satanik/ioc
```

if the package is not published use this in the composer file

```json
"repositories": {
  "satanik/ioc": {
    "type": "vcs",
    "url": "git@gitlab.com:satanik/laravel-ioc.git"
  },
  ...
},
...
"require": {
  ...
  "satanik/ioc": "<version constraint>",
```

or copy the repository to

```bash
<project root>/packages/Satanik/IoC
```

and use this code in the composer file

```json
"repositories": {
  "satanik/ioc": {
    "type": "path",
    "url": "packages/Satanik/IoC",
    "options": {
      "symlink": true
    }
  },
  ...
},
...
"require": {
  ...
  "satanik/ioc": "@dev",
```

## Usage

### Resolvable

A class needs to both implement the interface and use the trait `Resolvable` to return either the class string or an instance of the bound class

```php
use Satanik\IoC\Concerns\Resolveable;
use Satanik\IoC\Contracts\Resolveable as ResolveableContract;

class Custom implements ResolveableContract
{
    use Resolveable;
}
```

The service container would then bind something to the `Custom` class

```php
class CustomServiceProvider
{
    public function register()
    {
        $this->app->bind(Custom::class, function($app) {
            return new Custom();
        });
    }
}
```

After that anywhere in the code the following methods can be used

```php
Custom::bound(); // returns the class string <namspace>/Custom
Custom::resolve(); // same as resolve(Custom::class)
```

## Testing

``` bash
$ composer test
```

## Security

If you discover any security related issues, please email daniel@satanik.at instead of using the issue tracker.

## Credits

- [Daniel Satanik][link-author]

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.

[ico-license]: https://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat-square

[link-author]: https://satanik.at
